FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN apt-get update && apt-get install -y libmariadb3 && rm -rf /var/cache/apt /var/lib/apt/lists

# https://www.teamspeak.com/en/downloads/#server
ENV TSVERSION=3.13.7
ENV TS3SERVER_LICENSE=accept

RUN mkdir -p /app/code/web /run/nginx/lib /run/nginx/logs /app/data/ts/files && \
    wget https://files.teamspeak-services.com/releases/server/${TSVERSION}/teamspeak3-server_linux_amd64-${TSVERSION}.tar.bz2 && \
    tar xfvj teamspeak3-server_linux_amd64-${TSVERSION}.tar.bz2 -C /app/code && \
    rm teamspeak3-server_linux_amd64-$TSVERSION.tar.bz2 && \
    rm -rf /var/lib/nginx && ln -s /run/nginx/lib /var/lib/nginx && \
    ln -s /run/teamspeak/ts3db_mysql.ini /app/code/teamspeak3-server_linux_amd64/ts3db_mysql.ini && \
    ln -s /app/data/ts/files /app/code/teamspeak3-server_linux_amd64/files && \
    cp /app/code/teamspeak3-server_linux_amd64/redist/libmariadb.so.2 /app/code/teamspeak3-server_linux_amd64 && \
    chown -R cloudron.cloudron /app/code/teamspeak3-server_linux_amd64/

WORKDIR /app/code

# Supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
RUN sed -e 's,^chmod=.*$,chmod=0760\nchown=cloudron:cloudron,' -i /etc/supervisor/supervisord.conf

ADD service /app/code/service
RUN cd /app/code/service && npm i

ADD start.sh /app/code/

CMD [ "/app/code/start.sh" ]
