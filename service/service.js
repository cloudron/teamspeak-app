#!/usr/bin/env node

'use strict';

var execSync = require('child_process').execSync,
    express = require('express'),
    fs = require('fs'),
    path = require('path');

function checkTeamSpeak() {
    try {
        var out = execSync('supervisorctl status teamspeak', { encoding: 'utf-8' }).toString();
        if (out.indexOf('RUNNING') === -1) {
            console.log('service: teamspeak not running.', out);
            return false;
        }
    } catch (e) {
        console.error(e);
        return false;
    }

    return true;
}

function healthcheck(req, res, next) {
    if (checkTeamSpeak()) return res.status(200).send('OK');

    return res.status(503).send('NOT RUNNING');
}

// Setup the express server and routes
var app = express();
var router = new express.Router();

router.get   ('/api/healthcheck', healthcheck);

app.use(router);
app.use('/', express.static(path.join(__dirname, 'frontend')));

var server = app.listen(8000, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Service listening on http://%s:%s', host, port);
});
